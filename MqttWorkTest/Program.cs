﻿using System;
using NewLife.Log;
using NewLife.Caching;
using NewLife.Model;
using NewLife;
using NewLife.Remoting;
using XCode.DataAccessLayer;
using Stardust.Monitors;
using NewLife.MQTT;
using NewLife.Net;
using StackExchange.Redis;
namespace MqttWorkTest
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "Redis队列压力测试中";
            XTrace.UseConsole();
            
            var services = ObjectContainer.Current;
            services.AddSingleton(XTrace.Log);

            InitRedis(services);
            var host = services.BuildHost();
            host.Add<RedisPubWorker>();
            host.Add<RedisSubWorker>();

            host.Run();
        }
        static void InitRedis(IObjectContainer services)
        {
            ConnectionMultiplexer _conn = ConnectionMultiplexer.Connect("192.168.1.222:6379,allowAdmin=true");
            var _sub = _conn.GetSubscriber();
            services.AddSingleton(_sub);
        }
    }
}
