﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using NewLife.Log;
using NewLife.Model;
using StackExchange.Redis;
namespace MqttWorkTest
{
    public class RedisPubWorker : IHostedService
    {
     
      
        private readonly ISubscriber _subscriber;
        private readonly ILog _logger;

        public RedisPubWorker(ISubscriber subscriber, ILog logger)
        {
            _subscriber = subscriber;
            _logger = logger;
        }
        public Task StartAsync(CancellationToken cancellationToken)
        {
            var task = ExecuteAsync(cancellationToken);
            return task.IsCompleted ? task : Task.CompletedTask;
        }
        public Task StopAsync(CancellationToken cancellationToken) => Task.CompletedTask;
        protected async Task ExecuteAsync(CancellationToken stoppingToken)
        { 
           while(!stoppingToken.IsCancellationRequested)
            {
                await _subscriber.PublishAsync("MqttWorkTest.Timer", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff"));

            }
        }

    }
}
