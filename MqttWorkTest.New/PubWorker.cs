﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using NewLife.Log;
using NewLife.Model;
using NewLife.Caching;
namespace MqttWorkTest.New
{
    public class PubWorker : IHostedService
    {

        private readonly FullRedis _subscriber;
        private readonly ILog _logger;

        public PubWorker(FullRedis subscriber, ILog logger)
        {
            _subscriber = subscriber;
            _logger = logger;
        }
        public Task StartAsync(CancellationToken cancellationToken)
        {
            var task = ExecuteAsync(cancellationToken);
            return task.IsCompleted ? task : Task.CompletedTask;
        }
        public Task StopAsync(CancellationToken cancellationToken) => Task.CompletedTask;
        protected async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            var rdsQueue = _subscriber.GetReliableQueue<Object>("MqttWorkTest.Timer2");
            while (!stoppingToken.IsCancellationRequested)
            {
                 rdsQueue.Add(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff"));

            }
        }
    }

}
