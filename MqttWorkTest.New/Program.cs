﻿using System;
using NewLife.Log;
using NewLife.Caching;
using NewLife.Model;
using NewLife;
using NewLife.Remoting;
using XCode.DataAccessLayer;
using Stardust.Monitors;
using NewLife.MQTT;
using NewLife.Net;
namespace MqttWorkTest.New
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "Redis队列压力测试中";
            XTrace.UseConsole();

            var services = ObjectContainer.Current;
            services.AddSingleton(XTrace.Log);

            InitRedis(services);
            var host = services.BuildHost();
            host.Add<PubWorker>();
            host.Add<SubWorker>();

            host.Run();
        }
        static void InitRedis(IObjectContainer services)
        {
            var rds = new FullRedis {  };
            rds.Init("server=192.168.1.222;password=;db=3;timeout=5000");
            services.AddSingleton<ICache>(rds);
            services.AddSingleton(rds);
          
        }
    }
}
