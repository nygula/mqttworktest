﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using NewLife.Log;
using NewLife.Model;
using NewLife.Caching;
namespace MqttWorkTest.New
{
    public  class SubWorker : IHostedService
    {
        private readonly FullRedis _subscriber;
        private readonly ILog _logger;
        private RedisReliableQueue<String> _queue;

        public SubWorker(FullRedis subscriber, ILog logger)
        {
            _subscriber = subscriber;
            _logger = logger;
        }
        public Task StartAsync(CancellationToken cancellationToken)
        {
            var task = ExecuteAsync(cancellationToken);
            return task.IsCompleted ? task : Task.CompletedTask;
        }
        public Task StopAsync(CancellationToken cancellationToken) => Task.CompletedTask;
        protected async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            await Task.Yield();
            _queue = _subscriber.GetReliableQueue<String>("MqttWorkTest.Timer2");
            await _queue.ConsumeAsync<string>(area =>
            {
                XTrace.WriteLine($"{area}");
            }, stoppingToken,null);
        }
    }
}
